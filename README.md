# Linux 入门到精通

#### 介绍
Linux 入门到精通

| 目录    | 描述                                                      |
|-------|---------------------------------------------------------|
| /     | 根目录                                                     |
| /home | 普通用户的家目录                                                |
| /etc  | 大多数配置文件                                                 |
| /mnt  | 让用户临时挂载别的文件系统的目录，我们可以将光驱挂载在 /mnt/ 上，然后进入该目录就可以查看光驱里的内容。 | 

#### 一、目录操作命令【cd、pwd、find】
##### find（搜索目录）
```
find /bin -name 'a*'        查找 /bin 目录下的所有以 a 开头的文件或者目录
```

##### pwd（查看当前目录）
```
pwd                         显示当前位置路径
```

##### mkdir（新建目录）
```
mkdir  newxxxx               创建一个新的目录
mkdir -p a1/a2/a3/a4         连续创建多个新的目录
```

##### mv
```
mv elasticsearch-6.3.0/ elasticsearch		给目录重命名
mv elasticsearch-6.3.0 /home/				将当前目录下的 elasticsearch-6.3.0 复制到 /home 目录下
```

##### cp (复制文件或目录)：
```
cp /home/readme.txt ./readme.txt	将目录中的 /home/readme.txt 复制到当前目录下
cp /home/readme.txt ./readme.txt	将目录中的 /home/readme.txt 复制到当前目录下
```

#### 二、文件操作命令【touch、vim、tail -f、cat】
##### touch（新增文件）
```
touch  a.txt                在当前目录下创建名为a的txt文件（文件不存在），如果文件存在，将文件时间属性修改为当前系统时间
```

##### vi、vim（编辑文件）
```
  vi 文件名              //打开需要编辑的文件
```
- 进入后，操作界面有三种模式：命令模式（command mode）、插入模式（Insert mode）和底行模式（last line mode）
1. 命令模式 
	- 刚进入文件就是命令模式，通过方向键控制光标位置，
	- 使用命令"dd"     删除当前整行
	- 使用命令"/字段"  进行查找
	- 按"i"            在光标所在字符前开始插入
	- 按"a"            在光标所在字符后开始插入
	- 按"o"            在光标所在行的下面另起一新行插入
	- 按"："           进入底行模式
2. 插入模式 
	- 此时可以对文件内容进行编辑，左下角会显示 "-- 插入 --""
	- 按"ESC"进入底行模式
3. 底行模式 
	- 退出编辑：      :q  
	- 强制退出：      :q! 
	- 保存并退出：    :wq 

###### 操作步骤示例
1. 保存文件：按"ESC" -> 输入":" -> 输入"wq",回车     //保存并退出编辑
2. 取消操作：按"ESC" -> 输入":" -> 输入"q!",回车     //撤销本次修改并退出编辑

###### 补充
```
vim +10 filename.txt                   //打开文件并跳到第10行
vim -R /etc/passwd                     //以只读模式打开文件
```

##### 查看文件
```
cat a.txt          查看文件最后一屏内容
less a.txt         PgUp 向上翻页，PgDn 向下翻页，"q"退出查看
more a.txt         显示百分比，回车查看下一行，空格查看下一页，"q"退出查看
tail -100 a.txt    查看文件的后100行，"Ctrl+C"退出查看
```

#### 三、chmod (文件授权命令)
##### 文件权限 
```
普通授权   chmod +x a.txt    
8421法     chmod 777 a.txt     //1+2+4=7，"7"说明授予所有权限
```

#### 四、tar -zcvf 【打包】tar -zxvf【解压】
##### 打包文件 c 
```
tar -zcvf         打包压缩后的文件名 要打包的文件
```
参数说明：z：调用gzip压缩命令进行压缩; c：打包文件; v：显示运行过程; f：指定文件名;

- 示例：
```
tar -zcvf a.tar file1 file2,...      多个文件压缩打包
```

##### 解压文件 x 
```
tar -zxvf a.tar                      解包至当前目录
tar -zxvf a.tar -C /usr------        指定解压的位置
unzip test.zip                       解压*.zip文件 
unzip -l test.zip                    查看*.zip文件的内容 
```

#### 五、其他常用命令（11个）【find、grep、free、top、df、mount、uname、yum、wget、ftp、scp】
##### find 
```
find . -name "*.c"     			将目前目录及其子目录下所有延伸档名是 c 的文件列出来
find . -type f         			将目前目录其其下子目录中所有一般文件列出
find . -ctime -20      			将目前目录及其子目录下所有最近 20 天内更新过的文件列出
find /var/log -type f -mtime +7 -ok rm {} \;    	查找 /var/log 目录中更改时间在7日以前的普通文件，并在删除之前询问它们
find . -type f -perm 644 -exec ls -l {} \;      	查找前目录中文件属主具有读、写权限，并且文件所属组的用户和其他用户具有读权限的文件
find / -type f -size 0 -exec ls -l {} \;        	为了查找系统中所有文件长度为0的普通文件，并列出它们的完整路径
```

##### grep 文本搜索工具
```
grep -n "the" demo_file              显示匹配行以及行号
grep -i "the" demo_file              忽略大小写
grep -A 3 -i "example" demo_text     输出成功匹配的行，以及该行之后的三行
grep -r "ramesh" *                   在一个文件夹中递归查询包含指定字符串的文件
grep -v "the" demo_file              显示不包含匹配文本的所有行（相当于求返）
```

##### free 查看内存使用情况
说明：这个命令用于显示系统当前内存的使用情况，包括已用内存、可用内存和交换内存的情况
```
free -g            以 G 为单位输出内存的使用量，-g 为 GB，-m 为 MB，-k 为 KB，-b 为字节 
free -t            查看所有内存的汇总
```

##### top 	查看资源占用情况
```
top               显示当前系统中占用资源最多的一些进程, shift+m 按照内存大小查看
```

##### df 	显示磁盘使用情况
说明：显示文件系统的磁盘使用情况
```
df -h            一种易看的显示
```

##### mount 	
```
mount /dev/sdb1 /u01              挂载一个文件系统，需要先创建一个目录，然后将这个文件系统挂载到这个目录上
dev/sdb1 /u01 ext2 defaults 0 2   添加到 fstab 中进行自动挂载，这样任何时候系统重启的时候，文件系统都会被加载 
```

##### uname 
说明：uname可以显示一些重要的系统信息，例如内核名称、主机名、内核版本号、处理器类型之类的信息
```
uname -a
```

##### yum 
```
说明：安装插件命令
yum install httpd      使用 yum 安装  apache 
yum update httpd       更新           apache 
yum remove httpd       卸载/删除      apache 
```

##### wget 
说明：使用 wget 从网上下载软件、音乐、视频 
```
示例：wget http://prdownloads.sourceforge.net/sourceforge/nagios/nagios-3.2.1.tar.gz
```

##### 下载文件并以指定的文件名保存文件
```
wget -O nagios.tar.gz http://prdownloads.sourceforge.net/sourceforge/nagios/nagios-3.2.1.tar.gz
```

##### ftp 
```
ftp IP/hostname    访问ftp服务器
mls *.html -       显示远程主机上文件列表
```

##### scp 
```
scp /opt/data.txt  192.168.1.101:/opt/    将本地 opt 目录下的 data.txt 文件发送到 192.168.1.101 服务器的 opt 目录下
```

#### 六、系统操作命令
##### 查看进程
```
ps -ef         查看所有正在运行的进程
```

##### 结束进程
```
kill pid       杀死该pid的进程
kill -9 pid    强制杀死该进程
```